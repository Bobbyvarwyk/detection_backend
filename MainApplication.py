# import the necessary packages
from pyimagesearch.centroidtracker import CentroidTracker
from pyimagesearch.trackableobject import TrackableObject
from VideoCapture import VideoCapture
import numpy
import argparse
import imutils
import dlib
import cv2
from websocket import create_connection
import json

# define the argument parsers for run via command line, some are default set and not necessary to set
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", help="Path to Caffe deploy prototxt file", default="mobilenet_ssd/MobileNetSSD_deploy.prototxt") 
ap.add_argument("-m", "--model", help="path to Caffe pre-trained model", default="mobilenet_ssd/MobileNetSSD_deploy.caffemodel")
ap.add_argument("-i", "--input", type=str, help="path to input stream")
ap.add_argument("-c", "--confidence", type=float, default=0.4, help="minimum propability to filter weak detections")
ap.add_argument("-s", "--skip-frames", type=int, default=30, help="# of skip frames between detections")
ap.add_argument("-w", "--websocket", help="Path to websocket server", default="ws://127.0.0.1:3030")
ap.add_argument("-d", "--cameraID", type=str, help="Custom ID for the camera", required=True)
args = vars(ap.parse_args())

# define the classes of objects which MobileNetSSD can detect
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]

# load the MobileNetSSD model from resource
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
print("[INFO] Loading detection model from source...")

# function which sends data to the websocket
def socketHandler(humansDetected=int):
    try:
        socket = create_connection(args["websocket"] + "/params?ID=" + args["cameraID"] + "&type=input")
    except:
        print("[ERROR] Could not connect to the websocket")
        return False

    # check if the connection to the websocket was succesful
    if(socket.connected):
        # convert message data to JSON for Node websocket
        JSON_Msg = json.dumps({'customerCount': humansDetected})
        socket.send(JSON_Msg)
        print("[SUCCES] Message has been sent to websocket: ", JSON_Msg)
        return True

# main function which starts when script runs
def main():
    # initiate the variables needed in the script
    centroidTracker = CentroidTracker(maxDisappeared=40, maxDistance=50)
    personTrackers = []
    trackableObjects = {}
    totalFrames = 0

    # initiate empty frame dimensions
    H = None
    W = None

    # initiate old person detection count
    previousDetectionCount = 0

    # Start the video stream
    vStream = VideoCapture(args["input"]).start()

    # Loop through all frames of videostream when opened succesfully
    while vStream.stream.isOpened():
        # read the frames from the stream
        frame = vStream.frame

        # resize the frame to a maximum of 750 width to enhance processing time
        frame = imutils.resize(frame, width=750)
        # Convert the frame to RGB color for dlib
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)    

        # set new frame dimensions based on frame from stream
        if W is None or H is None:
            (H, W) = frame.shape[:2]

        # init the store variable for all bounding boxes
        rects = []

        if totalFrames % args["skip_frames"] == 0:
            # init new set of object trackers
            personTrackers = []
            # Convert the frame to a Blob to obtain detections
            blob = cv2.dnn.blobFromImage(frame,  0.007843, (W, H), 127.5)
            net.setInput(blob)
            detections = net.forward()

            # loop over all detections
            for i in numpy.arange(0, detections.shape[2]):
                # extract the confidence of the detection
                confidence = detections[0,0, i, 2]

                # filter out weak detections
                if confidence > args["confidence"]:
                    # extract the index of the class label from the detection
                    idxLbl = int(detections[0, 0, i, 1])

                    # filter out all detections which are not human
                    if CLASSES[idxLbl] != "person":
                        continue

                    # set the bounding boxes for the object, with coordinates
                    box = detections[0, 0, i, 3:7] * numpy.array([W, H, W, H])
                    (startX, startY, endX, endY) = box.astype("int")

                    # construct dlib object and start the dlib tracker
                    tracker = dlib.correlation_tracker()
                    rect = dlib.rectangle(startX, startY, endX, endY)
                    tracker.start_track(rgb, rect)

                    # add the tracker(s) to the trackers list
                    personTrackers.append(tracker)
        else:
            # loop through all trackers
            for tracker in personTrackers:
                # update trackers and get new positions
                tracker.update(rgb)
                pos = tracker.get_position()

                # get the position values
                startX = int(pos.left())
                startY = int(pos.top())
                endX = int(pos.right())
                endY = int(pos.bottom())

                # add the coordinates of the bounding boxes to the rects store variable
                rects.append((startX, startY, endX, endY))

        # update the objects new centroid
        objects = centroidTracker.update(rects)

        # loop over all tracked objects 
        for (objectID, centroid) in objects.items():
            # check if a trackable object exists with the current objectID
            to = trackableObjects.get(objectID, None)

            # if no existing trackable object, create one
            if to is None:
                to = TrackableObject(objectID, centroid)
            
            # store the trackable object in our trackableObjects list
            trackableObjects[objectID] = to

            # draw the ID on the center of the object in output screen
            text = "ID {}".format(objectID)
            cv2.putText(frame, text, (centroid[0] - 10, centroid[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        # send new amount of trackers to websocket
        if previousDetectionCount != int(len(personTrackers)):
            newDetectionCount = len(personTrackers)
            socketHandler(newDetectionCount)
            previousDetectionCount = newDetectionCount

        cv2.imshow("Person detector camera: " + args["cameraID"], frame)
        if cv2.waitKey(1) & 0xFF == ord("q"):
            vStream.stop()
            break
        # Up the total frames after each loop
        totalFrames += 1

cv2.destroyAllWindows()

# Code which initiates the main function
if __name__ == '__main__':
    main()