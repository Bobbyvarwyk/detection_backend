# import necessary libraries
from threading import Thread
import cv2

class VideoCapture:
    """
     Class which opens the videostream in cv2.videocapture on a single thread, for performance enhancement
    """

    def __init__(self, src=0):
        self.stream = cv2.VideoCapture(src)
        (self.grabbed, self.frame) = self.stream.read()
        self.stopped = False

    # start the video capture
    def start(self):
        Thread(target=self.getStream, args=()).start()
        return self
    
    def getStream(self):
        while not self.stopped:
            if not self.grabbed:
                self.stop()
            else:
                (self.grabbed, self.frame) = self.stream.read()
    
    # stop the video capture
    def stop(self):
        self.stopped = True
